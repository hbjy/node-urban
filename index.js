let fetch = require('node-fetch');
/**
 * Returns a JSON object from the Urban Dictionary API.
 * @public
 * @function
 * @param {String} phrase The phrase to search for.
 * @returns {Promise} The JSON object
 */
module.exports = (phrase) => { // Should be run as async
  return fetch(`http://api.urbandictionary.com/v0/define?term=${phrase}`)
    .then(r => r.json());
}