# node-urban
A Node.js Urban Dictionary API wrapper.

## Usage
```js
let def = urban('petella');
console.log(def);
```
```json
{
  "list": [
    { "definition": "What you [get smacked] in; [commonly] used when [enraged",
      "permalink": "http://petella.urbanup.com/7901125",
      "thumbs_up": 0,
      "sound_urls": [],
      "author": "notcharismatic",
      "word": "petella",
      "defid": 7901125,
      "current_vote": "",
      "written_on": "2014-09-16T00:00:00.000Z",
      "example": "If you don't [shut up], I'm [gonna] [smack you] right in the petella",
      "thumbs_down": 8 }
  ]
}
```